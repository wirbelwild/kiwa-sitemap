# Changes in Kiwa Sitemap v0.2

## 0.2.2 2021-01-22

### Fixed

-   Fixed error that could result in a notice when elements didn't exist.

## 0.2.1 2020-12-10

### Fixed

-   Fixed handling of UTF8 characters in URLs.
-   Fixed memory handling when it was set to `-1`.
-   Fixed handling of special chars in `alt` and `title` attributes.
-   Fixed wrong restore of previously crawled pages.

### Changed

-   Ignore PDF files from crawling.

## 0.2.0 2020-11-26

### Fixed

-   URLs with anchors or parameters will be handled correctly now.

### Changed 

-   The class `Sitemap` has been renamed to `SitemapCrawler`.
-   The class `DomPage` has been moved and renamed to `PageCrawler\CURLCrawler`.
-   Added support for PHP 8.

### Added 

-   The crawler can handle `<source />` tags and can find `.avif` files too.
-   A `BrowserShotCrawler` has been added. It is based on [Spaties Browsershot](https://github.com/spatie/browsershot) and allows crawling pages even with their JavaScript-generated content. 