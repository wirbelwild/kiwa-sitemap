# Changes in Kiwa Sitemap v2.0

## 2.0.0 2024-02-05

### Changed

-   PHP >=8.2 is now required.