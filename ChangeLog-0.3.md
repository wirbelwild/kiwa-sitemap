# Changes in Kiwa Sitemap v0.3

## 0.3.2 2021-12-28

### Fixed

-   Added attribute for PHP 8.1

## 0.3.1 2021-07-19

### Fixed

-   Fixed required version of `kiwa/console`.

## 0.3.0 2021-06-25

### Added

-   The library provides now its own commands, that can be used with the `kiwa/console` library.