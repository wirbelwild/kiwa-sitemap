<?php

/**
 * Kiwa Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\Sitemap\Command;

use BitAndBlack\Sitemap\Config\YamlConfig;
use BitAndBlack\Sitemap\SitemapCrawler;
use BitAndBlack\Sitemap\Writer\FileWriter;
use DOMException;
use Kiwa\Path;
use Kiwa\TemplateConsole\Writers\ConfigWriter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class SitemapCreateCommand extends Command
{
    /**
     * @param ConfigWriter $configWriter
     */
    public function __construct(
        private readonly ConfigWriter $configWriter,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('sitemap:create')
            ->setDescription('Creates a sitemap.xml file.')
            ->setHelp('This command will parse the whole website and create a sitemap.xml out of that.')
            ->addArgument(
                'url',
                InputArgument::OPTIONAL,
                'URL to the website that should be crawled. If empty, the crawler tries to use the <info>mainURL</info> from the Kiwa config.'
            )
            ->addOption(
                'crawling-limit',
                null,
                InputOption::VALUE_REQUIRED,
                'Defines how many pages should be crawled before the crawler stops.'
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws ExceptionInterface
     * @throws DOMException
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        /** @var int|float|string|null $mainUrl */
        $mainUrl = $input->getArgument('url');
        
        /** @var int|float|string|null $crawlingLimit */
        $crawlingLimit = $input->getOption('crawling-limit');

        if (null === $mainUrl) {
            /** @var string|null $mainUrl */
            $mainUrl = $this->configWriter->hasValue('mainURL')
                ? $this->configWriter->getValue('mainURL')
                : null
            ;
        }

        if (null === $mainUrl) {
            $output->writeln('<error>No main url defined.</error>');
            return Command::FAILURE;
        }

        $sitemap = new SitemapCrawler(
            new YamlConfig(Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'sitemap.yaml'),
            new FileWriter(Path::getCrawlerFolder())
        );
        
        if (null !== $crawlingLimit) {
            $sitemap->setCrawlingLimit((int) $crawlingLimit);
        }

        $sitemap->setOutput($output);
        $sitemap->createSitemap((string) $mainUrl);

        $output->writeln('<info>Finished crawling.</info>');

        $question = new Question('Do you want to update the <info>robots.txt</info> file now? (Defaults to <info>Yes, update it</info>): ', true);

        if (true === $helper->ask($input, $output, $question) && null !== $command = $this->getApplication()) {
            return $command->find('robotstxt:create')->run($input, $output);
        }

        return Command::SUCCESS;
    }
}
