[![PHP from Packagist](https://img.shields.io/packagist/php-v/kiwa/sitemap)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/kiwa/sitemap/v/stable)](https://packagist.org/packages/kiwa/sitemap)
[![Total Downloads](https://poser.pugx.org/kiwa/sitemap/downloads)](https://packagist.org/packages/kiwa/sitemap)
[![License](https://poser.pugx.org/kiwa/sitemap/license)](https://packagist.org/packages/kiwa/sitemap)

<p align="center">
    <a href="https://www.kiwa.io" target="_blank">
        <img src="https://www.kiwa.io/build/images/kiwa-logo.png" alt="Kiwa Logo" width="150">
    </a>
</p>

# Kiwa Sitemap

This library integrates the [Bit&Black Sitemap](https://packagist.org/packages/bitandblack/sitemap) into your [Kiwa](https://www.kiwa.io) website.

It creates a `sitemap.xml` by parsing the whole website including all language versions and all images. If multiple language versions are found, multiple `xml` files will be written.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/kiwa/sitemap). Add it to your project by running `$ composer require kiwa/sitemap`. 

## Usage 

After adding this library to your project, the [Kiwa Console](https://packagist.org/packages/kiwa/console) contains a new command: `sitemap:create`.

When running `bin/console sitemap:create`, the website defined in `config.php` under `mainURL` will be crawled completely. After that, a `sitemap.xml` file will be stored in the `public` folder and depending on the languages found some additional language files like `sitemap-en.xml` will be stored there, too.

## Help 

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).
