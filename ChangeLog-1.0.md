# Changes in Kiwa Sitemap v1.0

## 1.0.0 2022-02-21

### Added 

-   The option `crawling-limit` has been added to tell, how many pages should be crawled before the crawler stops.

### Changed

-   The core functionality has been moved to [Bit&Black Sitemap](https://packagist.org/packages/bitandblack/sitemap).