<?php

/**
 * Kiwa Sitemap.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\Sitemap\Tests\Command;

use BitAndBlack\Helpers\FileSystemHelper;
use Kiwa\Path;
use Kiwa\Sitemap\Command\SitemapCreateCommand;
use Kiwa\TemplateConsole\Writers\ConfigWriter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Tester\CommandTester;

class SitemapCreateCommandTest extends TestCase
{
    public static function tearDownAfterClass(): void
    {
        FileSystemHelper::deleteFolder(Path::getConfigFolder());
        FileSystemHelper::deleteFolder(Path::getPublicFolder());
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $configWriter = new ConfigWriter();
        
        $sitemapCreateCommand = new SitemapCreateCommand($configWriter);
        
        $helperSet = new HelperSet();
        $helperSet->set(new QuestionHelper());
        
        $sitemapCreateCommand->setHelperSet($helperSet);
        
        $commandTester = new CommandTester($sitemapCreateCommand);
        $commandTester
            ->setInputs([''])
            ->execute([
                'url' => 'https://www.kiwa.io',
                '--crawling-limit' => 2,
            ])
        ;
        
        $commandTester->assertCommandIsSuccessful();
        
        $output = $commandTester->getDisplay();

        self::assertStringContainsString(
            'Stopped parsing because of page count limit',
            $output
        );

        self::assertStringContainsString(
            'Finished crawling',
            $output
        );
    }
}
